package pl.ti.Technologie.Internetowe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.ti.Technologie.Internetowe.model.Car;
import pl.ti.Technologie.Internetowe.model.Reserve;
import pl.ti.Technologie.Internetowe.service.CarService;

import java.util.List;

@RestController
public class CarRest {

    final private CarService carService;

    @Autowired
    public CarRest(CarService carService) {
        this.carService = carService;
    }

    @PostMapping("/index")
    public @ResponseBody
    List<Car> search(Car car) {
        System.out.println(car.getType());
        System.out.println(car.getPrice());
        return carService.searchCar(car);
    }
}
