package pl.ti.Technologie.Internetowe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.ti.Technologie.Internetowe.model.Car;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car,Integer> {

    public List<Car> findByPrice(int price);
    public List<Car> findByType(String type);
    public List<Car> findByPriceAndType(int price, String type);
}
