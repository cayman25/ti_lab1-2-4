package pl.ti.Technologie.Internetowe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ti.Technologie.Internetowe.model.Car;
import pl.ti.Technologie.Internetowe.repository.CarRepository;

import java.util.List;

@Service
public class CarService {

    final private CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> searchCar(Car car) {
        if(car.getPrice() !=0 && !car.getType().isEmpty()) {
            return carRepository.findByPriceAndType(car.getPrice(),car.getType());
        }
        else if(car.getType().isEmpty()){
            return carRepository.findByPrice(car.getPrice());
        }
        else {
            return carRepository.findByType(car.getType());
        }
    }
}
