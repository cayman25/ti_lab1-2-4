package pl.ti.Technologie.Internetowe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.ti.Technologie.Internetowe.model.Reserve;
import pl.ti.Technologie.Internetowe.repository.ReserveRepository;

import java.time.LocalDate;

@Service
public class ReserveService {

    final ReserveRepository reserveRepository;

    @Autowired
    public ReserveService(ReserveRepository reserveRepository) {
        this.reserveRepository = reserveRepository;
    }

    public String addReserve(Reserve reserve) {
        if (validReserve(reserve) == 0) {
            reserveRepository.save(reserve);
            return "Zarezerwowane";
        } else
            return "Błędna data";
    }

    private int validReserve(Reserve reserve) {
        LocalDate start = LocalDate.parse(reserve.getStartReservation());
        LocalDate end = LocalDate.parse(reserve.getEndReservation());

        if (start.isAfter(end) || start.isBefore(LocalDate.now()) || end.isBefore(LocalDate.now())) {
            return 1;
        } else
            return 0;
    }

}
